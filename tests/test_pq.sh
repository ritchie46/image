#!/bin/sh -eu

> test.c cat << EOF
#include <stdio.h>
#include <libpq-fe.h>

int main() {
	printf("Hello, World\n");
	PQconnectStart("testtt");
}
EOF

> build_test.sh cat << EOF
#!/bin/sh
musl-gcc -c -I/musl/include test.c
musl-gcc -static -o test test.o -L /musl/lib/ -lpq
./test
EOF

chmod 0755 build_test.sh

echo "Building a binary with Postgres 11.7"

docker run -it --rm \
	-w "/workdir" \
	-v "$PWD:/workdir" \
	registry.gitlab.com/rust_musl_docker/image/base:openssl-1.1.1d_postgres-11.7 \
	./build_test.sh

echo "Building a binary with Postgres 12.0"

docker run -it --rm \
	-w "/workdir" \
	-v "$PWD:/workdir" \
	registry.gitlab.com/rust_musl_docker/image/base:openssl-1.1.1d_postgres-12.0 \
	./build_test.sh
