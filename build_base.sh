#!/bin/sh -xe

OPENSSL_VER=${1:-1.1.1d}
POSTGRES_VER=${2:-11.7}

echo "Building a base image with OpenSSL ${OPENSSL_VER} and Postgres ${POSTGRES_VER}"

# zlib seems to update so seldom that
# it's version is hardcoded in the BaseDockerfile.template
# as zlib 1.2.11

TEMP_DOCKERFILE="$(mktemp -d)/BaseDockerfile"
REPO="registry.gitlab.com/rust_musl_docker/image/base"
TAG="openssl-${OPENSSL_VER}_postgres-${POSTGRES_VER}"
FULL_TAG="${REPO}:${TAG}"

< BaseDockerfile.template \
sed "s@OPENSSL_VER@$OPENSSL_VER@g" | \
sed "s@POSTGRES_VER@$POSTGRES_VER@g" > "$TEMP_DOCKERFILE"

docker build -f "$TEMP_DOCKERFILE" -t "$FULL_TAG" .
docker login registry.gitlab.com -u gitlab-ci-token -p "$CI_JOB_TOKEN"
docker push "$FULL_TAG"
docker tag "$FULL_TAG" "${REPO}:latest"
docker push "${REPO}:latest"
